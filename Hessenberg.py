import numpy as np

def norm(x):
    return sum(elem ** 2 for elem in x) ** 0.5

def house(x):
    n = len(x)
    nu = norm(x)
    v = x
    if nu != 0:
        b = x[0] + np.sign(x[0]) * nu
        v[1:n] = v[1:n] / b
    v[0] = 1
    return v


def row_house(a, v):
    beta = -2.0 / np.dot(np.transpose(v), v)
    vm = np.array([v]).transpose()
    w = beta * np.dot(a.transpose(), vm)
    a += np.dot(vm, w.transpose())
    return a

def col_house(a, v):
    beta = -2.0 / np.dot(v.T, v)
    vm = np.array([v]).transpose()
    w = beta * np.dot(a, vm)
    a += np.dot(w, vm.transpose())
    return a


def make_h(a):
    for i in range(0, len(a[0])-1):
        a[i+2:len(a), i] = np.zeros(len(a) - (i+2))
    return a


def hessenberg(a):
    n = len(a)
    v = np.zeros(n)
    for k in range(n-2):
        v[k+1:n] = house(a[k+1:n, k].copy())
        a[k+1:n, k:n] = row_house(a[k+1:n, k:n].copy(), v[k+1:n].copy())
        a[:n, k+1:n] = col_house(a[:n, k+1:n].copy(), v[k+1:n].copy())
        a[k+2:n, k] = v[k+2:n]
    return make_h(a)

