import sys

if sys.version_info[0] != 3:
    print("This script requires Python version 3")
    sys.exit(1)

import numpy as np
import Tests as test

np.set_printoptions(suppress=False, linewidth=160, precision=17)


def main():

    test_num = 10
    n = 6

    test.test(test_num, n)
if __name__ == '__main__':
    main()


