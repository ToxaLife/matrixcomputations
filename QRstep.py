import numpy as np
import scipy as sc

def norm(x):
    return sum(elem**2 for elem in x) ** 0.5

def house(x):
    n = len(x)
    nu = norm(x)
    v = x
    if nu != 0:
        b = x[0] + np.sign(x[0]) * nu
        v[1:n] = v[1:n] / b
    v[0] = 1
    return v


def row_house(a, v):
    beta = -2.0 / np.dot(np.transpose(v), v)
    vm = np.array([v]).transpose()
    w = beta * np.dot(a.transpose(), vm)
    a += np.dot(vm, w.transpose())
    return a

def col_house(a, v):
    beta = -2.0 / np.dot(v.T, v)
    vm = np.array([v]).transpose()
    w = beta * np.dot(a, vm)
    a += np.dot(w, vm.transpose())
    return a


def qr_step(h):
    n = len(h)
    m = n - 1

    s = h[m-1, m-1] + h[n-1, n-1]
    t = h[m-1, m-1] * h[n-1, n-1] - h[m-1, n-1] * h[n-1, m-1]

    x = h[0, 0] * h[0, 0] + h[0, 1] * h[1, 0] - s * h[0, 0] + t
    y = h[1, 0] * (h[0, 0] + h[1, 1] - s)
    z = h[0, 1] * h[2, 1]

    for k in range(n-4):
        v = house(np.array([x, y, z]).T)
        h[k+1:k+4, k+1:n] = row_house(h[k+1:k+4, k+1:n].copy(), v.copy())
        r = min(k+4, n)
        h[0:r, k+1:k+4] = col_house(h[0:r, k+1:k+4].copy(), v.copy())
        x = h[k+2, k+1]
        y = h[k+3, k+1]
        if k < n - 4:
            z = h[k+4, k+1]

    v = house(np.array([x, y]).T)
    h[n-2:n, n-2:n] = row_house(h[n-2:n, n-2:n].copy(), v.copy())
    h[0:n, n-2:n] = col_house(h[0:n, n-2:n].copy(), v.copy())

    return h



def qrpoly(h):
    n = len(h)
    hh = h[n-2:n, n-2:n]
    tr_hh = hh[0, 0] + hh[1, 1]
    det_hh = hh[0, 0] * hh[1, 1] - hh[0, 1] * hh[1, 0]

    if tr_hh ** 2 > 4.0 * det_hh:

        l_hh = np.zeros(2)

        l_hh[0] = (tr_hh + (tr_hh**2 - 4.0 * det_hh)**0.5) / 2.0
        l_hh[1] = (tr_hh - (tr_hh**2 - 4.0 * det_hh)**0.5) / 2.0

        if abs(l_hh[0] - h[n-1, n-1]) < abs(l_hh[1] - h[n-1, n-1]):
            l_hh[1] = l_hh[0]
        else:
            l_hh[0] = l_hh[1]

        b = -l_hh[0] - l_hh[1]
        c = l_hh[0] * l_hh[1]

    else:
        b = -tr_hh
        c = det_hh

    return b, c


def make_h(a):
    for i in range(0, len(a[0])-1):
        a[i+2:len(a), i] = np.zeros(len(a) - (i+2))
    return a

def qrstep(h):
    b, c = qrpoly(h.copy())

    c1 = np.dot(h[0:3, 0:2], h[0:2, 0])
    c1[0:2] = c1[0:2] + b * h[0:2, 0]
    c1[0] = c1[0] + c
    v = house(c1.copy())

    h[0:3] = row_house(h[:3].copy(), v.copy())
    h[:, :3] = col_house(h[:, :3].copy(), v.copy())

    n = len(h)
    v = np.zeros(n)
    for k in range(n-2):
        v[k+1:n] = house(h[k+1:n, k].copy())
        h[k+1:n, k:n] = row_house(h[k+1:n, k:n].copy(), v[k+1:n].copy())
        h[:n, k+1:n] = col_house(h[:n, k+1:n].copy(), v[k+1:n].copy())
        h[k+2:n, k] = v[k+2:n]

    return make_h(h)


def super_qr(h):
    n = len(h)

    tol = sc.linalg.norm(h, 'fro') * 1e-14
    k = 0
    while n > 2:
        if abs(h[n-1, n-2]) < tol:
            h[n-1, n-2] = 0.0
            n -= 1
        elif abs(h[n-2, n-3]) < tol:
            h[n-2, n-3] = 0.0
            n -= 2
        else:
            h[0:n, 0:n] = qrstep(h[0:n, 0:n].copy())
            k += 1

    return h



def new_qr(h):
    n = len(h)
    p = n
    tol = sc.linalg.norm(h, 'fro') * 1e-8
    while p > 1:
        q = p - 1

        s = h[q-1, q-1] + h[p-1, p-1]
        t = h[q-1, q-1] * h[p-1, p-1] - h[q-1, p-1] * h[p-1, q-1]

        x = h[0, 0] * h[0, 0] + h[0, 1] * h[1, 0] - s * h[0, 0] + t
        y = h[1, 0] * (h[0, 0] + h[1, 1] - s)
        z = h[1, 0] * h[2, 1]

        for k in range(p-4):
            v = house(np.array([x, y, z]).T)
            r = max(0, k)
            h[k:k+3, r:n] = row_house(h[k:k+3, r:n].copy(), v.copy())
            r = min(k+4, p)
            h[0:r+1, k:k+3] = col_house(h[0:r+1, k:k+3].copy(), v.copy())
            x = h[k+1, k]
            y = h[k+2, k]
            if k - 1 < p - 4:
                z = h[k+3, k]

        v = house(np.array([x, y]).T)
        h[q-1:p, p-3:n] = row_house(h[q-1:p, p-3:n].copy(), v.copy())
        h[0:p, p-2:p] = col_house(h[0:p, p-2:p].copy(), v.copy())

        # print(h)
        # print("\n")
        print(q, p, h[p-1, p-2], h[p-2, p-3])
        if abs(h[p-1, q-1]) < tol:
            print("here1")
            h[p-1, q-1] = 0.0
            p -= 1
            q = p - 1
        elif abs(h[p-2, q-2]) < tol:
            print("here2")
            h[p-1, q-1] = 0.0
            p -= 2
            q = p - 1

    return h