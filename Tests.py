import numpy as np
from scipy.linalg import hessenberg
import Hessenberg as hb
import QRstep as qrs


def test(test_num, n):

    error_hg = []
    error_diag = []
    for i in range(0, test_num):
        print("-"*24*n + "TEST #" + str(i+1) + "-"*24*n)
        a = np.random.randn(n, n)
        a_sym = (a + a.T) / 2.0
        print("A")
        print(a)
        hb_matr = hb.hessenberg(a.copy())
        hb_scipy = hessenberg(a.copy())
        print("HB")
        print(hb_matr)
        # print("SCIPY")
        # print(hb_scipy)
        error_hg.append(np.linalg.norm(hb_matr - hb_scipy, 'fro'))
        print(">error_hb" + str(error_hg[i]))

        diag_matr = hb.hessenberg(a_sym.copy())
        diag_scipy = hessenberg(a_sym.copy())
        # print("A_sym")
        # print(a_sym)
        # print("Diag_matr")
        # print(diag_matr)
        error_diag.append(np.linalg.norm(diag_matr - diag_scipy, 'fro'))
        print(">error_diag" + str(error_diag[i]))


        #print("res")
        #print(qrs.super_qr(hb_matr))

    print("AVG error_hb: " + str(np.mean(error_hg)))
    print("AVG error_diag: " + str(np.mean(error_diag)))




